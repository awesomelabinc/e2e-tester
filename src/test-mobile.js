
import wd from "wd";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);
chai.should();
chaiAsPromised.transferPromiseness = wd.transferPromiseness;

const browser = wd.promiseChainRemote("0.0.0.0", 4723);

const testCase = (process.argv[8] != undefined ? process.argv[8] : "tests/test-mobile.json");
const tests = require("./../" + testCase);

Object.keys(tests).forEach((descKey) => {
    const descValue = tests[descKey];

    switch(descKey) {
		case "open":
			before(function(done) {
                browser
                    .init(descValue)
                    .setImplicitWaitTimeout(3000)
                    .nodeify(done);  //same as : .then(function() { done(); });
            });
		break;
		default:
            describe(descKey, () => {
                Object.keys(descValue).forEach((itKey) => {
					it(itKey, function(done) {
						Object.keys(itValue).forEach((key, index) => {
							const value = itValue[key];
							switch (key) {
								case "type": {
									browser.elementByXPath("//*[@content-desc='" + value + "']", function(err, res) {
										res.sendKeys("username").back()
									});
									break;
								}
								case "click": {
									browser.elementByXPath("//*[@text='" + value + "']", function(err, res){
										res.click()
									});
									break;
								}
								case "find content": {
									browser.elementByXPath("//*[@text='" + value + "']", function(err, res){
										//res.should.eventually.exist.nodeify(done);
										if(err)
											throw new Error(err);
										res.nodeify(done);
										//res.nodeify(done);
									})
								}
							}
						});
						
					});
                });
            });
        break;
    }

});


after(function(done) {
      browser
        .quit()
        .nodeify(done);
    });


describe("test mobile", function(){
	it("should load", function(done) {
      browser
		.sleep(5000)
		.nodeify(done);
    });

	
	it("should show wrong name or password", function(done) {
      	browser
		.elementByXPath("//*[@content-desc='user name']", function(err, res){
			res.sendKeys("username").back()
			.elementByXPath("//*[@content-desc='password']", function(err, res){
				res.sendKeys("password").back()
				.elementByXPath("//*[@text='SIGN IN']", function(err, res){
					res.click()
					.elementByXPath("//*[@text='wrong name or passwords']", function(err, res){
						//res.should.eventually.exist.nodeify(done);
						if(err)
							throw new Error(err);
						res.nodeify(done);
						//res.nodeify(done);
					})
					
				});
				
			})

		})
		
    });
});
/*
browser.init(desired).then(function() {
	return browser
		.sleep(5000)
		.elementByXPath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.EditText[1]").sendKeys("test")
		.elementByXPath("//*[@text='UNILEVES']")
		.fin(function() {
			return browser.quit();
		});
}).done();
*/