import webdriver from 'selenium-webdriver';
const expect = require('expect');

let browser = (process.argv[7] != undefined ? process.argv[7] : "phantomjs");

let testCase = (process.argv[8] != undefined ? process.argv[8] : "tests/test-case.json");

const driver = new webdriver.Builder().forBrowser(browser).build();
const By = webdriver.By;
const test = require('./test-utils')(driver);
const tests = require("./../" + testCase);



/*
before((done) => {
	test.navigate('http://localhost:3000/')
	driver.navigate().to("http://localhost:3002/")
		.then(() => done());
});
*/
Object.keys(tests).forEach((descKey) => {
	const descValue = tests[descKey];

	switch(descKey) {
		case "open":
		case "goto":
		case "go to":
			before((done) => {
			driver.navigate().to(descValue)
				.then(() => done());
			});
		break;
		default:
			describe(descKey, () => {
				Object.keys(descValue).forEach((itKey) => {
					const itValue = descValue[itKey];
					const keyCount = Object.keys(itValue).length;
					if (keyCount === 0) {
						it(itKey);
						return;
					}
					it(itKey, (done) => {
						Object.keys(itValue).forEach((key, index) => {
							const value = itValue[key];
							switch (key) {
								/*
								case "find content":
									{
										Object.keys(value).forEach((expectKey) => {
											const expectValue = value[expectKey];
											test.getText(By.css(expectKey))
												.then((str) => {
													expect(str).toBe(expectValue);
												}).catch((error) => {
													console.log(error);
												});
										});
										break;
									}
								*/
								case "pause":
								case "sleep":
									{
										driver.sleep(value);
										break;
									}
								case "input":
								case "enter":
								case "type":
									{
										Object.keys(value).forEach((typeKey) => {
											if (typeKey.charAt(0) === '.') {
												const typeValue = value[typeKey];
												test.clearValue(By.css(typeKey));
												//test.setValue(By.name(typeKey), "");
												test.setValue(By.css(typeKey), typeValue);
											} else {
												const typeValue = value[typeKey];
												test.clearValue(By.name(typeKey));
												//test.setValue(By.name(typeKey), "");
												test.setValue(By.name(typeKey), typeValue);
											}
										});
										break;
									}
								case "should have":
								case "look for":
								case "find content":
								case "find text":
									{
										test.getTextAnywhere().then((content) => {
											const startText = content.indexOf(value);
											if(startText > -1) {
												const endText = value.length
												const str = content.slice(startText, startText + endText);
												expect(str).toBe(value);
											} else {
												throw new Error(value + " not found");
											}

										});
										break;
									}
								case "find contents":
									{
										// value should be  array
										var count = value.length;
										for(var i = 0; i < count; i++) {
											//
											var val = value[i];
											test.getTextAnywhere().then((content) => {
												const startText = content.indexOf(val);
												if(startText > -1) {
													const endText = val.length
													const str = content.slice(startText, startText + endText);
													expect(str).toBe(val);
												} else {
													throw new Error(val + " not found");
												}

											});
										}
										break;

									}
								case "click element with":
								case "click":
									{
										test.click(By.css(value));
										break;
									}
								case "wait redirection to":
								case "wait":
								case "wait url":
									{
										test.wait(value);
										break;
									}
								case "find element with":
								case "find element":
									{
										test.waitElement(By.css(value));
										break;
									}
								case "find elements":
									{
										// value should be  array
										var count = value.length;
										for(var i = 0; i < count; i++) {
											//
											var val = value[i];
											test.waitElement(By.css(val));
										}
										break;

									}
								default:
									{
										expect(key).toBe("listed as test tag").
										then(() => done());
									}
							}
							if (keyCount === index + 1) {
								test.getCurrentUrl().
								then(() => done());
							}
						});
					});
				});
			});
		break;

	}
});
after((done) => {
	test.quit().
	then(() => done());
});
