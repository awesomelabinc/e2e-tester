const until = require('selenium-webdriver').until;

module.exports = driver => ({
    quit: function() {
        return driver.quit();
    },
    navigate: function(url) {
        return driver.navigate().to(url);
    },
    waitElement: function(selector) {
        return driver.wait(until.elementLocated(selector), 2 * 1000);
    },
    getTextAnywhere: function() {
        return driver.getPageSource();
    },
    getCurrentUrl: function() {
        return driver.getCurrentUrl();
    },
    wait: function(selector) {
        return driver.wait(until.urlContains(selector));
    },
    getText: function(selector) {
        return driver.findElement(selector).getText();
    },
    getValue: function(selector) {
        return driver.findElement(selector).getAttribute('value');
    },
    setValue: function(selector, value) {
        return driver.findElement(selector).sendKeys(value);
    },
	clearValue: function(selector) {
		return driver.findElement(selector).clear();
	},
    click: function(selector) {
        return driver.findElement(selector).click();
    },
});
