# TEST KIT

# e2e website tester
using selenium-webdriver


# Changelog

## v.1.3
```
//added type/enter/input using className
"input": {
    ".input-class-name": "hello"
}
//added sleep/pause
"sleep": 5000 // ms
```

## v.1.2
added pagespeed insight
```bash
#linux
npm run test-pagespeed <google.com> <minimum pagespeed number>
```

## v.1.1
added multiple find content/element
    - ex. find contents: ["array", "of", "content"]
    - ex. find elements: [".button1", ".button2", ".button3"]
removed download test runner

## v1.0
initial release










# download nodejs
```bash
# linux
sudo apt-get install nodejs
sudo apt-get install npm
```
# download phantomjs
```bash
# linux
curl --output /usr/local/bin/phantomjs https://s3.amazonaws.com/circle-downloads/phantomjs-2.1.1
chmod +x /usr/local/bin/phantomjs

```


# download chromedriver
```bash
# linux - should have chrome installed
curl --output /usr/local/bin/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/2.27/chromedriver_linux64.zip
unzip /usr/local/bin/chromedriver_linux64.zip -d /usr/local/bin/

```

## command
```bash

# inside project folder
npm run build
npm test <chrome or phantomjs| optional> <local directory of test case starting in base folder | optional>

```
### sample test run
```bash
# run test locally
npm test chrome test-case.json

```

## sample json test case
```javascript

{
	"open": "https://www.youtube.com/",
    "in youtube": {
        "if search rick astley in youtube, i will find Never Gonna Give You Up": {
            "type": {
                "search_query": "Rick Astley"
            },
            "click": ".search-button",
            "wait": "https://www.youtube.com/results?search_query=Rick+Astley",
            "find content": "Rick Astley - Never Gonna Give You Up"
        },
        "if i click the first link i will be rick rolled and find RickAstleyVEVO": {
            "click": "h3.yt-lockup-title > a",
            "wait": "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
            "find content": "RickAstleyVEVO"
        }
    },
    "another test": {
        ...
    }
}

....
{
    "open": "http://www.ncounter.io",
    "in nCounter Website":
    {
        
        "Check for content":
                {
                    "find contents": ["EMAIL", "PASSWORD"],
                    "find elements": [".button-login", "input[name='email']"]
                }
    }
}

```

## starting url commands
```

"open": <website starting url>

```

## available action commands
```

"click" - mouse click the element
    - ex.
        "click": ".search-button" // .search-button - .class or #id https://saucelabs.com/resources/articles/selenium-tips-css-selectors
"type" - type text inside input boxes
    - ex.
        "type": {
                "search_query": "Rick Astley" // search_query - attribute "name" of input tag, Rick Astley - text to type
        }
    - ex. // can do multiple type in a single action
        "type": { // action
            "email": "juandelacruz@test.io", // name - input name ie <input name="email" />
            "password": "test" // name - input name ie <input name="password" />
        }

"wait" - wait for the browser to redirect or for the url to change
    - ex.
        "wait": "https://www.youtube.com/watch?v=dQw4w9WgXcQ"

```

## available tests commands

```

// 1 test per test case
"find element": ".u-show" // .class or #id
"find content": "juandelacruz" // find text in page

// multiple test
"find elements": [".u-show", ".button-login"] // find multiple .class or #id
"find contents": ["content1", "content2"] // find multiple text in page


```


### commands
```

open
    goto
    go to

input
    enter
    type

find content
    look for
    find text
    should have
wait
    wait redirection to
    
find element
    find element with

find elements

find contents

```
