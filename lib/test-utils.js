'use strict';

var until = require('selenium-webdriver').until;

module.exports = function (driver) {
    return {
        quit: function quit() {
            return driver.quit();
        },
        navigate: function navigate(url) {
            return driver.navigate().to(url);
        },
        waitElement: function waitElement(selector) {
            return driver.wait(until.elementLocated(selector), 2 * 1000);
        },
        getTextAnywhere: function getTextAnywhere() {
            return driver.getPageSource();
        },
        getCurrentUrl: function getCurrentUrl() {
            return driver.getCurrentUrl();
        },
        wait: function wait(selector) {
            return driver.wait(until.urlContains(selector));
        },
        getText: function getText(selector) {
            return driver.findElement(selector).getText();
        },
        getValue: function getValue(selector) {
            return driver.findElement(selector).getAttribute('value');
        },
        setValue: function setValue(selector, value) {
            return driver.findElement(selector).sendKeys(value);
        },
        clearValue: function clearValue(selector) {
            return driver.findElement(selector).clear();
        },
        click: function click(selector) {
            return driver.findElement(selector).click();
        }
    };
};