"use strict";

var _wd = require("wd");

var _wd2 = _interopRequireDefault(_wd);

var _chai = require("chai");

var _chai2 = _interopRequireDefault(_chai);

var _chaiAsPromised = require("chai-as-promised");

var _chaiAsPromised2 = _interopRequireDefault(_chaiAsPromised);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.use(_chaiAsPromised2.default);
_chai2.default.should();
_chaiAsPromised2.default.transferPromiseness = _wd2.default.transferPromiseness;

var browser = _wd2.default.promiseChainRemote("0.0.0.0", 4723);

var testCase = process.argv[8] != undefined ? process.argv[8] : "tests/test-mobile.json";
var tests = require("./../" + testCase);

Object.keys(tests).forEach(function (descKey) {
	var descValue = tests[descKey];

	switch (descKey) {
		case "open":
			before(function (done) {
				browser.init(descValue).setImplicitWaitTimeout(3000).nodeify(done); //same as : .then(function() { done(); });
			});
			break;
		default:
			describe(descKey, function () {
				Object.keys(descValue).forEach(function (itKey) {
					it(itKey, function (done) {
						Object.keys(itValue).forEach(function (key, index) {
							var value = itValue[key];
							switch (key) {
								case "type":
									{
										browser.elementByXPath("//*[@content-desc='" + value + "']", function (err, res) {
											res.sendKeys("username").back();
										});
										break;
									}
								case "click":
									{
										browser.elementByXPath("//*[@text='" + value + "']", function (err, res) {
											res.click();
										});
										break;
									}
								case "find content":
									{
										browser.elementByXPath("//*[@text='" + value + "']", function (err, res) {
											//res.should.eventually.exist.nodeify(done);
											if (err) throw new Error(err);
											res.nodeify(done);
											//res.nodeify(done);
										});
									}
							}
						});
					});
				});
			});
			break;
	}
});

after(function (done) {
	browser.quit().nodeify(done);
});

describe("test mobile", function () {
	it("should load", function (done) {
		browser.sleep(5000).nodeify(done);
	});

	it("should show wrong name or password", function (done) {
		browser.elementByXPath("//*[@content-desc='user name']", function (err, res) {
			res.sendKeys("username").back().elementByXPath("//*[@content-desc='password']", function (err, res) {
				res.sendKeys("password").back().elementByXPath("//*[@text='SIGN IN']", function (err, res) {
					res.click().elementByXPath("//*[@text='wrong name or passwords']", function (err, res) {
						//res.should.eventually.exist.nodeify(done);
						if (err) throw new Error(err);
						res.nodeify(done);
						//res.nodeify(done);
					});
				});
			});
		});
	});
});
/*
browser.init(desired).then(function() {
	return browser
		.sleep(5000)
		.elementByXPath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.EditText[1]").sendKeys("test")
		.elementByXPath("//*[@text='UNILEVES']")
		.fin(function() {
			return browser.quit();
		});
}).done();
*/