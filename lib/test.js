'use strict';

var _seleniumWebdriver = require('selenium-webdriver');

var _seleniumWebdriver2 = _interopRequireDefault(_seleniumWebdriver);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = require('expect');

var browser = process.argv[7] != undefined ? process.argv[7] : "phantomjs";

var testCase = process.argv[8] != undefined ? process.argv[8] : "tests/test-case.json";

var driver = new _seleniumWebdriver2.default.Builder().forBrowser(browser).build();
var By = _seleniumWebdriver2.default.By;
var test = require('./test-utils')(driver);
var tests = require("./../" + testCase);

/*
before((done) => {
	test.navigate('http://localhost:3000/')
	driver.navigate().to("http://localhost:3002/")
		.then(() => done());
});
*/
Object.keys(tests).forEach(function (descKey) {
	var descValue = tests[descKey];

	switch (descKey) {
		case "open":
		case "goto":
		case "go to":
			before(function (done) {
				driver.navigate().to(descValue).then(function () {
					return done();
				});
			});
			break;
		default:
			describe(descKey, function () {
				Object.keys(descValue).forEach(function (itKey) {
					var itValue = descValue[itKey];
					var keyCount = Object.keys(itValue).length;
					if (keyCount === 0) {
						it(itKey);
						return;
					}
					it(itKey, function (done) {
						Object.keys(itValue).forEach(function (key, index) {
							var value = itValue[key];
							switch (key) {
								/*
        case "find content":
        	{
        		Object.keys(value).forEach((expectKey) => {
        			const expectValue = value[expectKey];
        			test.getText(By.css(expectKey))
        				.then((str) => {
        					expect(str).toBe(expectValue);
        				}).catch((error) => {
        					console.log(error);
        				});
        		});
        		break;
        	}
        */
								case "sleep":
									{
										driver.sleep(value);
										break;
									}
								case "input":
								case "enter":
								case "type":
									{
										Object.keys(value).forEach(function (typeKey) {
											if (typeKey.charAt(0) === '.') {
												var typeValue = value[typeKey];
												test.clearValue(By.css(typeKey));
												//test.setValue(By.name(typeKey), "");
												test.setValue(By.css(typeKey), typeValue);
											} else {
												var _typeValue = value[typeKey];
												test.clearValue(By.name(typeKey));
												//test.setValue(By.name(typeKey), "");
												test.setValue(By.name(typeKey), _typeValue);
											}
										});
										break;
									}
								case "should have":
								case "look for":
								case "find content":
								case "find text":
									{
										test.getTextAnywhere().then(function (content) {
											var startText = content.indexOf(value);
											if (startText > -1) {
												var endText = value.length;
												var str = content.slice(startText, startText + endText);
												expect(str).toBe(value);
											} else {
												throw new Error(value + " not found");
											}
										});
										break;
									}
								case "find contents":
									{
										// value should be  array
										var count = value.length;
										for (var i = 0; i < count; i++) {
											//
											var val = value[i];
											test.getTextAnywhere().then(function (content) {
												var startText = content.indexOf(val);
												if (startText > -1) {
													var endText = val.length;
													var str = content.slice(startText, startText + endText);
													expect(str).toBe(val);
												} else {
													throw new Error(val + " not found");
												}
											});
										}
										break;
									}
								case "click element with":
								case "click":
									{
										test.click(By.css(value));
										break;
									}
								case "wait redirection to":
								case "wait":
								case "wait url":
									{
										test.wait(value);
										break;
									}
								case "find element with":
								case "find element":
									{
										test.waitElement(By.css(value));
										break;
									}
								case "find elements":
									{
										// value should be  array
										var count = value.length;
										for (var i = 0; i < count; i++) {
											//
											var val = value[i];
											test.waitElement(By.css(val));
										}
										break;
									}
								default:
									{
										expect(key).toBe("listed as test tag").then(function () {
											return done();
										});
									}
							}
							if (keyCount === index + 1) {
								test.getCurrentUrl().then(function () {
									return done();
								});
							}
						});
					});
				});
			});
			break;

	}
});
after(function (done) {
	test.quit().then(function () {
		return done();
	});
});