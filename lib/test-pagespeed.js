'use strict';

process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';
var pagespeedInsights = require('pagespeed-insights');

var colors = {
	Reset: "\x1b[0m",
	Bright: "\x1b[1m",
	Dim: "\x1b[2m",
	Underscore: "\x1b[4m",
	Blink: "\x1b[5m",
	Reverse: "\x1b[7m",
	Hidden: "\x1b[8m",
	fg: {
		Black: "\x1b[30m",
		Red: "\x1b[31m",
		Green: "\x1b[32m",
		Yellow: "\x1b[33m",
		Blue: "\x1b[34m",
		Magenta: "\x1b[35m",
		Cyan: "\x1b[36m",
		White: "\x1b[37m",
		Crimson: "\x1b[38m"
	},
	bg: {
		Black: "\x1b[40m",
		Red: "\x1b[41m",
		Green: "\x1b[42m",
		Yellow: "\x1b[43m",
		Blue: "\x1b[44m",
		Magenta: "\x1b[45m",
		Cyan: "\x1b[46m",
		White: "\x1b[47m",
		Crimson: "\x1b[48m"
	}
};

var page = process.argv[2];
var pageMin = process.argv[3];
var exitOnFail = process.argv[4];
var apiKey = process.env.PAGESPEED_API_KEY;

console.log(process.env.PAGESPEED_API_KEY);

var opts = {
	url: "http://" + page,
	apiKey: apiKey,
	strategy: 'desktop'
};

pagespeedInsights(opts, function (err, data) {
	var pageSpeed = void 0;
	for (var obj1 in data) {
		if (data.hasOwnProperty(obj1)) {
			console.log("pageSpeed ", data[obj1].pageSpeed);
			if (data[obj1].pageSpeed) {
				pageSpeed = data[obj1].pageSpeed;
			}
			console.log("PAGE SPEED:", " ", colors.Underscore, pageSpeed + "/100", colors.Reset);
			//console.log(data[obj1]);
			var rulesInfo = data[obj1].rulesInfo;
			for (var i = 0; i < rulesInfo.length; i++) {
				//console.log(colors.Reset);
				if (rulesInfo[i].ruleImpact === 0) console.log(colors.fg.Green, colors.Dim, rulesInfo[i].localisedRuleName + " - " + rulesInfo[i].ruleImpact);else {
					console.log(colors.Reset);
					console.log(colors.fg.Red, rulesInfo[i].localisedRuleName + " - " + rulesInfo[i].ruleImpact);
					//console.log(colors.Reset);
					console.log(colors.fg.White, "\t=>" + rulesInfo[i].summary);
					//console.log(colors.fg.White, "\t\t" + rulesInfo[i].urlBlocks);
					for (var j = 0; j < rulesInfo[i].urlBlocks.length; j++) {
						console.log(colors.fg.White, "\t=>" + JSON.stringify(rulesInfo[i].urlBlocks[j]));
						//for (var h = 0; h < rulesInfo[i].urlBlocks.length; j++) {
						//for (var obj2 in data) {
						//	console.log(colors.fg.White, "\t=>" + JSON.stringify(rulesInfo[i].urlBlocks[j][obj2]));
						//}
					}
				}
			}
		}
	}

	if (parseInt(pageSpeed) < pageMin) {
		console.log(colors.fg.Red, colors.Dim, "\nDid not reach pagespeed minimum. Check details above");
		console.log(colors.Reset);

		if (exitOnFail !== undefinded) {
			console.log(exitOnFail);
			console.log(colors.Reset);
			process.exit(1);
		}
	} else {
		//console.log(colors.fg.Green, colors.Dim, "\nWebsite passed");

	}
	console.log(colors.Reset);
});